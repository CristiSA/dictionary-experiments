package ro.wantsome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        List<String> allLines = new ArrayList<>();

        allLines = Utils.readAllLinesFromResourcesFile("dex.txt");

        Set<String> wordsSet = Utils.removeDuplicates(allLines);

        // Using Scanner for Getting Input from User
//        Scanner in = new Scanner(System.in);
        BufferedReader in =
                new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.println("Menu:\n\t" +
                    "1. Search\n\t" +
                    "2. Find all palindromes\n\t" +
                    "0. Exit");

            String userMenuSelection = in.readLine();
            System.out.println("Read: " + userMenuSelection);

            // Exit if user selected 0
            if (userMenuSelection.equals("0")){
                System.out.println("Exiting...");
                break;
            }

            switch (userMenuSelection) {
                case "1":
                    System.out.println("Searching");

                    String userGivenWord = in.readLine();

                    if ("".equals(userGivenWord))
                        break;

                    Set<String> resultedWords =
                            Utils.findWordsThatContain(wordsSet, userGivenWord);

                    for (String line : resultedWords) {
                        System.out.println(line);
                    }

                    break;
                case "2":
                    System.out.println("Palindromes");
                    Set<String> palindromes =
                            Utils.findPalindromes(wordsSet);

                    for (String line : palindromes) {
                        System.out.println(line);
                    }
                    break;
                default:
                    System.out.println("Please select a valid option");
            }


        }
    }
}
