package ro.wantsome;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class UtilsTests {
    @Test
    public void testPalindromeFinderIdentifiesPalindromes() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertTrue(palindromes.contains("asa"));
    }

    @Test
    public void testPalindromeFinder_FindsNothing() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("acasa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(0, palindromes.size());
    }

    @Test
    public void testPalindromeFinder_FindsMultiple() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        inputSet.add("ata");
        inputSet.add("cojoc");
        inputSet.add("penar");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(3, palindromes.size());
    }
    @Test
    public void testPalindromeFinder_IsResilient() {
        Set<String> palindromes = Utils.findPalindromes(null);
        Assert.assertEquals(0, palindromes.size());
    }
}
